$(document).ready(function() {
	$("input").mouseup(function() {
		$(this).select();
	}).keyup(function(event) {
		var code = event.keyCode || event.which;
		if ( code === 9 || code === 16 || code === 8 || code === 46 || code === 37 || code === 38 )
		{
			return true;
		}
		else
		{
			var cell = $( this ).closest("tr td.active");
			$(cell).next().find("input").focus().select();
		};
	});

	$("button#check").click(function() {
		var solution = "";
		$("td:nth-child(5) div input").each(function() {
			solution += $(this).val();
		});
		if (solution.toLowerCase() == "komputerowybiathlon")
		{
			$("p#notice").empty();
			$("p#notice").append("Brawo! Rozwiązałeś krzyżówkę");
		}
		else
		{
			$("p#notice").empty();
			$("p#notice").append("Error! Error! Hasło nieprawidłowe");
		};
	});
});